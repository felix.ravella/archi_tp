import { Controller, Get } from '@nestjs/common';
import { Public } from 'src/auth/public.decorator';
import { SandwichService } from './sandwich.service';

@Controller('sandwich')
export class SandwichController {
  constructor(private sandwichService: SandwichService) {}

  @Public()
  @Get()
  getSandwiches() {
      return this.sandwichService.getAll();
  }

}
