-- CreateTable
CREATE TABLE "Sandwich" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "bread" TEXT NOT NULL,
    "meat" TEXT NOT NULL,
    "salade" BOOLEAN NOT NULL DEFAULT true
);
